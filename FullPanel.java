import javax.swing.*;
import java.awt.*;
import java.util.*;

public class FullPanel extends JApplet{

    private AnalyzePanel AnalyzePanel;
    private SettingsPanel settingsPanel;
    private HelpPanel helpPanel;
    private Vector<Analyze> analyze;
    public String dispText;

    private JTabbedPane tabs;

    private int APPLET_WIDTH = 1000;
    private int APPLET_HEIGHT = 750;
    private String nameOfFile;


    public void init() {
    	analyze = new Vector<Analyze>();
        settingsPanel = new SettingsPanel(AnalyzePanel, analyze, dispText);
        AnalyzePanel = new AnalyzePanel(settingsPanel,analyze);
        helpPanel = new HelpPanel();

        tabs = new JTabbedPane();
        tabs.addTab("Analysis", AnalyzePanel);
        tabs.addTab("History", settingsPanel);
        tabs.addTab("Help", helpPanel);
       // tabs.addTab("Help", settingsPanel);
        //sets up tabs for program

        getContentPane().add(tabs);
        setSize(APPLET_WIDTH,APPLET_HEIGHT);



    }
}
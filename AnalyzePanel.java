import java.awt.*;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.border.*;
import java.util.*;

public class AnalyzePanel extends JPanel {

    private JTextArea disp;
    private JTextArea nameListView;
    private JButton addButton;
    private JButton processButton;
    private JPanel blankPanel;
    private JPanel buttonPanel;
    private JPanel dispPanel;
    private JPanel listPanel;
    private Font fHuge;
    private Font fLarge;
    private Font fMedium;
    private Font fSmall;
 
    private JFileChooser fileChooser;

    private SettingsPanel settingsPanel;

    private Vector<Analyze> analysisList;
    
    private Vector<String> listOfFileNames;

    public String dispText;
    
    private int count;

    public AnalyzePanel(SettingsPanel settingsPanel, Vector<Analyze> analysisList){

        this.settingsPanel = settingsPanel;
        this.analysisList = analysisList;
        listOfFileNames = new Vector<String>();

        setBackground(Color.WHITE);
        count = 0;

        dispText = "Add a file to get started";

        fHuge = new Font("SansSerif",Font.PLAIN,48);
        fLarge = new Font("SansSerif",Font.PLAIN,32);
        fMedium = new Font("SansSerif",Font.PLAIN,22);
        fSmall = new Font("SansSerif",Font.PLAIN,18);

        nameListView = new JTextArea();
        JScrollPane listScroll = new JScrollPane(nameListView);
        listScroll.setPreferredSize(new Dimension(400,600));
        nameListView.setBackground(Color.WHITE);
        nameListView.setText("No files selected");
        nameListView.setFont(fSmall);
        
        disp = new JTextArea();
        JScrollPane textScroll = new JScrollPane(disp);
        textScroll.setPreferredSize(new Dimension(600,600)); //Sets a fixed size for the text view
        disp.setBackground(Color.WHITE);
        disp.setText(dispText);
        
        
        addButton = new JButton("Add File");
        addButton.setBackground(Color.WHITE);
        processButton = new JButton("Process");
        processButton.setBackground(Color.WHITE);
        
        processButton.setEnabled(false);

        addButton.addActionListener(new ButtonListener());
        processButton.addActionListener(new ButtonListener());
        
        fileChooser = new JFileChooser();

        disp.setFont(fLarge);
        addButton.setFont(fLarge);
        processButton.setFont(fLarge);

        blankPanel = new JPanel();
        buttonPanel = new JPanel();
        buttonPanel.add(addButton);
        buttonPanel.add(processButton);
        dispPanel = new JPanel();
        dispPanel.add(textScroll);
        listPanel = new JPanel();
        listPanel.add(listScroll);

        setLayout(new BorderLayout());
        add(blankPanel,BorderLayout.NORTH);
        add(dispPanel,BorderLayout.EAST);
        add(listPanel,BorderLayout.WEST);
        add(buttonPanel,BorderLayout.SOUTH);

    }

    private class ButtonListener implements ActionListener{

        public void actionPerformed(ActionEvent clicked) {
        		if (clicked.getSource() == addButton) {
        			int returnVal = fileChooser.showOpenDialog(addButton);
        			if (returnVal == JFileChooser.APPROVE_OPTION) {
        				File file = fileChooser.getSelectedFile();
        				boolean fileNameExists = false;
        				String fileName = file.getPath();
        				for (int fileNo = 0; fileNo < listOfFileNames.size(); fileNo++) {
        					if (fileName.equals(listOfFileNames.get(fileNo))) {
        						fileNameExists = true;
        					}
        				}
        				String selectedText = new String();
        				String messageText = new String();
        				if (fileNameExists) {
        					messageText = "A file with the name " + fileName + " already exists\n\n";
        				}
        				else {
        					listOfFileNames.add(fileName);
        				}
        				disp.setFont(fLarge);
        				selectedText = selectedText + "File(s) selected:\n\n";
        				for (int fileNo = 0; fileNo < listOfFileNames.size(); fileNo++) {
        					selectedText = selectedText + listOfFileNames.get(fileNo) + "\n";
        				}
        				messageText = messageText + "Click Process to continue";
        				nameListView.setText(selectedText);
        				disp.setText(messageText);
        				processButton.setEnabled(true);
        			}

            }
        		
            else if(clicked.getSource() == processButton) {
            		disp.setFont(fMedium);
            		dispText = new String();
            		for (int fileNo = 0; fileNo < listOfFileNames.size(); fileNo++) {
            			Analyze analysisEntry = new Analyze(listOfFileNames.get(fileNo));
            			analysisList.add(analysisEntry);
            			dispText = dispText + analysisEntry.toString();
            		}
            		listOfFileNames.clear();
            		nameListView.setText("No files selected");
            		disp.setText(dispText);
            		addButton.setEnabled(true);
            		processButton.setEnabled(false);
            		String historyText = new String();
            		for (int fileNo = 0; fileNo < analysisList.size(); fileNo++) {
            			historyText = historyText + analysisList.get(fileNo).toString();
            		}
            		settingsPanel.dispText = historyText;
        		
        		
            }







            updateUI();
        }
    }






}
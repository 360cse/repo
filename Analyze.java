import java.util.Arrays;
import java.math.*;
import java.util.*;
import java.io.*;

public class Analyze {

	private String fileName;
	private String fileDate;
	private int numLines = 0;
	private int numBlankLines = 0;
	private int numSpaces = 0;
	private int numOfChars = 0;
	private int avgCharsPerLine = 0;
	private WordAnalysis wordTrack;
	
	public Analyze(String nameOfFile) {
		
		fileName = nameOfFile;
		Date analysisDate = new Date();
		fileDate = analysisDate.toString();
		
		try {
			FileReader fReader = new FileReader(nameOfFile);
			BufferedReader bReader = new BufferedReader(fReader);
			String line = bReader.readLine();
			while (line != null) {
				numLines++;
				
				int spacesInLine = 0;
				
				for (int c = 0; c < line.length(); c++) {
					if (line.charAt(c) == ' ') {
						spacesInLine++;
					}
				}
				if (line.length() == 0 || line.length() == spacesInLine) {
					numBlankLines++;
				}
				else {
					for (int c = 0; c < line.length(); c++) {
						if (line.charAt(c) == ' ') {
							numSpaces++;
							}
						numOfChars++;
					}
				}
				line = bReader.readLine();
			}
			
			avgCharsPerLine = numOfChars / numLines;
			
			bReader.close();
		}
		catch (FileNotFoundException excptn){
			System.out.println("Unable to open " + nameOfFile);
		}
		catch (IOException excptn){
			System.out.println("Error reading " + nameOfFile);
		}
		
		wordTrack = new WordAnalysis(nameOfFile);
		
	}
	
	public String toString() {
		return ("File name: " + fileName + "\n" + "Date: " + fileDate + "\n" + "Number of lines: " + String.valueOf(numLines) + "\n" + "Number of blank lines: " + String.valueOf(numBlankLines) + "\n" + "Number of spaces: " + String.valueOf(numSpaces) + "\n" + "Average number of characters per line: " + String.valueOf(avgCharsPerLine) + "\n" + wordTrack.toString() + "\n");
	}
	
	public int getNumLines() {
		return numLines;
	}
	
	public int getNumBlankLines() {
		return numBlankLines;
	}
	
	public int getNumSpaces() {
		return numSpaces;
	}
	
	public int getAvgCharsPerLine() {
		return avgCharsPerLine;
	}
	
	public int getNumWords() {
		return wordTrack.getNumWords();
	}
	
	public int getAvgWordLength() {
		return wordTrack.getAvgWordLength();
	}
	
}
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;

public class HelpPanel extends JPanel{

    private JTextPane card;
    private JButton prevButton;
    private JButton nextButton;
    private JPanel blankPanel;
    private JPanel buttonPanel;
    private JPanel cardPanel;
    private Font fHuge;
    private Font fLarge;
    private Font fSmall;
    private String[] helpNotes;
    private int noteNum;



    private String cardText;

    public HelpPanel(){
    	
    		noteNum = 0;
    		
    		helpNotes = new String[3];
    		
    		// Add help text to each of these notes. Describe what users must do to select and analyze files, etc.
    		
    		helpNotes[0] = "Add files to analyze \n\nUnder the 'Analysis' tab:\nClick 'Add File' to open up a dialog where you can select a text file to analyze. After selecting a file, its name is displayed in a list of files to be analyzed. Repeat this process to add additional files.";
    		helpNotes[1] = "Run and view file analysis \n\nUnder the 'Analysis' tab:\nAfter selecting files, click 'Process' to analyze the files. The results of the analysis are then displayed.";
    		helpNotes[2] = "View file history and averages \n\nUnder the 'History' tab:\nView a running history of all file analyses, as well as averages across all files.";

        setBackground(Color.WHITE);

        cardText = helpNotes[0];

        fHuge = new Font("SansSerif",Font.PLAIN,48);
        fLarge = new Font("SansSerif",Font.PLAIN,32);
        fSmall = new Font("SansSerif",Font.PLAIN,16);

        card = new JTextPane();
        JScrollPane cardScroll = new JScrollPane(card);
        cardScroll.setPreferredSize(new Dimension(700,600)); //sets a fixed size for the card
        card.setBackground(Color.WHITE);
        card.setText(cardText);
        prevButton = new JButton("Prev");
        prevButton.setBackground(Color.WHITE);
        nextButton = new JButton("Next");
        nextButton.setBackground(Color.WHITE);

        prevButton.addActionListener(new ButtonListener());
        nextButton.addActionListener(new ButtonListener());

        card.setFont(fLarge);
        prevButton.setFont(fLarge);
        nextButton.setFont(fLarge);

        blankPanel = new JPanel();
        buttonPanel = new JPanel();
        buttonPanel.add(prevButton);
        buttonPanel.add(nextButton);
        cardPanel = new JPanel();
        cardPanel.add(cardScroll);

        setLayout(new BorderLayout());
        add(blankPanel,BorderLayout.NORTH);
        add(cardPanel,BorderLayout.CENTER);
        add(buttonPanel,BorderLayout.SOUTH);

    }

    private class ButtonListener implements ActionListener{

        public void actionPerformed(ActionEvent clicked){

            if (clicked.getSource() == prevButton) {
            	
            	if (noteNum == 0) {
            		noteNum = 2;
            	}
            	else {
            		noteNum = noteNum - 1;
            	}
            	
            	card.setText(helpNotes[noteNum]);

            } 
            else if(clicked.getSource() == nextButton) {
            	
            	if (noteNum == 2) {
            		noteNum = 0;
            	}
            	else {
            		noteNum = noteNum + 1;
            	}
            	
            	card.setText(helpNotes[noteNum]);
            	
            }







            updateUI();
        }
    }






}
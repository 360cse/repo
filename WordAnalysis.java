import java.io.*;
import java.util.*;

public class WordAnalysis {
	private int numWords = 0;
	private int avgWordLength = 0;
	private int totalWordLengths = 0;
	private String words[];
	private String mostCommonWords[];
	
	public WordAnalysis(String nameOfFile) {
		
		try {
			FileReader fReader = new FileReader(nameOfFile);
			BufferedReader bReader = new BufferedReader(fReader);
			
			String line = bReader.readLine();
			
			while (line != null) {
				line = line.replaceAll("'s", "");
				line = line.replaceAll("['/()?!$,&-;:\"\\.^*%_+=#@~`<>|{}\\[\\]]", "");
				String newWords[]= line.split(" ");
				if (!line.isEmpty()) {
					numWords = numWords + newWords.length;
				}
				String temp[] = words;
				if (words == null) {
					words = newWords;
				}
				else {
					words = new String[numWords];
					System.arraycopy(temp, 0, words, 0, temp.length);
					if (!line.isEmpty()) {
						System.arraycopy(newWords, 0, words, temp.length, newWords.length);
					}
				}
				
				line = bReader.readLine();
			}
			
			bReader.close();
			
			for (int i = 0; i < words.length; i++) {
				words[i] = words[i].toLowerCase();
				totalWordLengths = totalWordLengths + words[i].length();
			}
			
			avgWordLength = totalWordLengths / words.length;
			
			int mostCommonWordCount = 0;
			int numOfDifferentCommonWords = 0;
			
			for (int w = 0; w < words.length; w++) {
				int currentWordCount = 0;
				for (int p = 0; p < words.length; p++) {
					if (words[p].equals(words[w])) {
						currentWordCount++;
					}
				}
				if (mostCommonWordCount == 0) {
					mostCommonWordCount = currentWordCount;
					numOfDifferentCommonWords = 1;
					mostCommonWords = new String[1];
					mostCommonWords[0] = words[w];
				}
				else {
					if (currentWordCount > mostCommonWordCount) {
						mostCommonWordCount = currentWordCount;
						numOfDifferentCommonWords = 1;
						mostCommonWords = new String[1];
						mostCommonWords[0] = words[w];
						
					}
					else if (currentWordCount == mostCommonWordCount) {
						boolean newCommonWord = true;
						for (int m = 0; m < mostCommonWords.length; m++) {
							if (words[w].equals(mostCommonWords[m])) {
								newCommonWord = false;
							}
						}
						if (newCommonWord == true) {
							numOfDifferentCommonWords++;
							String commonTemp[] = mostCommonWords;
							mostCommonWords = new String[numOfDifferentCommonWords];
							for (int t = 0; t < commonTemp.length; t++) {
								mostCommonWords[t] = commonTemp[t];
							}
							mostCommonWords[numOfDifferentCommonWords - 1] = words[w];
						}
					}
				}
			}
			
		}
		catch (FileNotFoundException excptn){
			System.out.println("Unable to open " + nameOfFile);
		}
		catch (IOException excptn){
			System.out.println("Error reading " + nameOfFile);
		}
	}
	
	public String toString() {
		String output = ("Number of words: " + String.valueOf(numWords) + "\n" + "Average word length: " + String.valueOf(avgWordLength) + "\n" + "Most common word(s): ");
		for (int w = 0; w < mostCommonWords.length; w++) {
			output = output + mostCommonWords[w];
			if (w < (mostCommonWords.length - 1)) {
				output = output + ", ";
			}
			else {
				output = output + "\n";
			}
		}
		return output;
	}
	
	public int getNumWords() {
		return numWords;
	}
	
	public int getAvgWordLength() {
		return avgWordLength;
	}
	
}
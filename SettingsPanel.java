import java.awt.*;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.border.*;

//import AnalyzePanel.ButtonListener;

import java.util.*;

public class SettingsPanel extends JPanel{

    private JLabel creditsLabel;
    private JLabel txtLabel;
    private JLabel disclaimerLabel;
    private JPanel creditsPanel;
    private JPanel northPanel;
    private JPanel buttonPanel;
    private JPanel txtPanel;
    private JPanel infoPanel;
    private JTextArea info;
    private JButton update;
    private Font fLarge;
    private Font fMedium;
    private Font fSmall;
    
    private int avgAllNumLines = 0;
	private int avgAllNumBlankLines = 0;
	private int avgAllNumSpaces = 0;
	private int avgAllNumOfChars = 0;
	private int avgAllAvgCharsPerLine = 0;
	private int avgAllNumWords = 0;
	private int avgAllAvgWordLength = 0;

    private String[] txtArray;
    private Vector txtList;
    private Vector<Analyze> analysisList;
    private Vector<String> listOfFileNames;
    private AnalyzePanel analyzePanel;

    private String infoText;
    public String dispText;

    public SettingsPanel(AnalyzePanel analyzePanel, Vector<Analyze> analysisList, String dispText){

        this.txtList = txtList;
        this.analyzePanel = analyzePanel;
        this.analysisList = analysisList;
        listOfFileNames = new Vector<String>();
        this.dispText = dispText;

        fLarge = new Font("SansSerif",Font.PLAIN,32);
        fMedium = new Font("SansSerif",Font.PLAIN,26);
        fSmall = new Font("SansSerif",Font.PLAIN,16);
        
        update = new JButton("Update");
        update.setBackground(Color.WHITE);
        update.setFont(fMedium);

        creditsLabel = new JLabel("For CSE360 Fall 2017");
        creditsLabel.setFont(fSmall);
        disclaimerLabel = new JLabel("Created by Chad Allen, Zayne Bamond, and Bryan Vu.");
        disclaimerLabel.setFont(fSmall);
        txtLabel =  new JLabel("Past information analyzed:");
        txtLabel.setFont(fLarge);

        infoText = "";
        info = new JTextArea();
        info.setText(infoText);
        info.setBackground(Color.WHITE);
        info.setFont(fSmall);
        
        JScrollPane textScroll = new JScrollPane(info);
        textScroll.setPreferredSize(new Dimension(600,600));

        creditsPanel = new JPanel();
        creditsPanel.setLayout(new GridLayout(2,1));
        creditsPanel.add(disclaimerLabel);
        creditsPanel.add(creditsLabel);


        buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());

        txtPanel = new JPanel();
        txtPanel.add(txtLabel);
        
        infoPanel = new JPanel();
        infoPanel.add(textScroll);
        infoPanel.add(update);

        northPanel = new JPanel();
        northPanel.setLayout(new BorderLayout());
        northPanel.add(txtPanel, BorderLayout.NORTH);
        northPanel.add(infoPanel, BorderLayout.CENTER);
        
        setLayout(new BorderLayout());
        add(northPanel, BorderLayout.CENTER);
        add(creditsPanel, BorderLayout.SOUTH);

        txtArray = new String[400];
        
        update.addActionListener(new ButtonListener());


    }

    
    
    
    private class ButtonListener implements ActionListener{

        public void actionPerformed(ActionEvent clicked) {
        		if (clicked.getSource() == update) {
        		for (int fileNo = 0; fileNo < analysisList.size(); fileNo++) {
        			avgAllNumLines += analysisList.get(fileNo).getNumLines();
        			avgAllNumBlankLines += analysisList.get(fileNo).getNumBlankLines();
        			avgAllNumSpaces += analysisList.get(fileNo).getNumSpaces();
        			avgAllAvgCharsPerLine += analysisList.get(fileNo).getAvgCharsPerLine();
        			avgAllNumWords += analysisList.get(fileNo).getNumWords();
        			avgAllAvgWordLength += analysisList.get(fileNo).getAvgWordLength();
        			}
        		
        		avgAllNumLines /= analysisList.size();
    			avgAllNumBlankLines /= analysisList.size();
    			avgAllNumSpaces /= analysisList.size();
    			avgAllAvgCharsPerLine /= analysisList.size();
    			avgAllNumWords /= analysisList.size();
    			avgAllAvgWordLength /= analysisList.size();
        		
        		infoText = dispText;
        		info.setText(infoText);


        		}
        	


            updateUI();

        
        }
    }
}
    
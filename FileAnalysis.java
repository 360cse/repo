import java.io.*;
import java.util.*;

public class FileAnalysis {
	
	private String fileName;
	private String fileDate;
	private int numLines = 0;
	private int numBlankLines = 0;
	private int numSpaces = 0;
	private int numOfChars = 0;
	private int avgCharsPerLine = 0;
	private WordAnalysis wordTrack;
	
	public FileAnalysis(String nameOfFile) {
		
		fileName = nameOfFile;
		Date analysisDate = new Date();
		fileDate = analysisDate.toString();
		
		try {
			FileReader fReader = new FileReader(nameOfFile);
			BufferedReader bReader = new BufferedReader(fReader);
			String line = bReader.readLine();
			while (line != null) {
				numLines++;
				
				int spacesInLine = 0;
				
				for (int c = 0; c < line.length(); c++) {
					if (line.charAt(c) == ' ') {
						spacesInLine++;
					}
				}
				if (line.length() == 0 || line.length() == spacesInLine) {
					numBlankLines++;
				}
				else {
					for (int c = 0; c < line.length(); c++) {
						if (line.charAt(c) == ' ') {
							numSpaces++;
							}
						else if (line.charAt(c) != '!' && line.charAt(c) != '$' && line.charAt(c) != '?' && line.charAt(c) != '.' && line.charAt(c) != ',' && line.charAt(c) != '"' && line.charAt(c) != '\'' && line.charAt(c) != ':' && line.charAt(c) != ';' && line.charAt(c) != '&' && line.charAt(c) != '%' && line.charAt(c) != '(' && line.charAt(c) != ')' && line.charAt(c) != '*') {
							numOfChars++;
						}
					}
				}
				line = bReader.readLine();
			}
			
			avgCharsPerLine = numOfChars / numLines;
			
			bReader.close();
		}
		catch (FileNotFoundException excptn){
			System.out.println("Unable to open " + nameOfFile);
		}
		catch (IOException excptn){
			System.out.println("Error reading " + nameOfFile);
		}
		
		wordTrack = new WordAnalysis(nameOfFile);
		
	}
	
	public String toString() {
		return ("File name: " + fileName + "\n" + "Date: " + fileDate + "\n" + "Number of lines: " + String.valueOf(numLines) + "\n" + "Number of blank lines: " + String.valueOf(numBlankLines) + "\n" + "Number of spaces: " + String.valueOf(numSpaces) + "\n" + "Average number of characters per line: " + String.valueOf(avgCharsPerLine) + "\n" + "\n");
	}
	
}